import React, { Component } from 'react'
import Axios from 'axios'
import uuid from 'uuid'
import './DadJokes.css'
import Joke from './Joke'

class DadJokes extends Component{
    static defaultProps = {
        NUM_JOKES: 3
    }
    constructor(props){
        super(props)
        this.state = {
            jokes: JSON.parse(window.localStorage.getItem("jokes") || "[]"),
            loading: true
        }
        this.seenJokes = new Set(JSON.parse(window.localStorage.getItem("jokes") || "[]").map(joke => joke.text))
        this.getJokes = this.getJokes.bind(this)
        this.handleVote = this.handleVote.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }
    async getJokes(){
        let newJokes = []
        let n = this.props.NUM_JOKES
        let i = 0
        while(i < n){
            let fetchedJoke = await Axios.get("https://icanhazdadjoke.com/", {headers: {"Accept": "application/json"}});
            let id = uuid()
            if (!this.seenJokes.has(fetchedJoke.data.joke)){
                let joke = {votes :0 ,id: id, text:fetchedJoke.data.joke, upvote : () => {this.handleVote(id, 1)}, downvote : () => {this.handleVote(id, -1)}}
                newJokes.push(joke);
                this.seenJokes.add(fetchedJoke.data.joke)
                i = i+1;    //DO NOT REMOVE THIS
            } else {
                console.log("Found a duplicate")
            }
        }
        this.setState({jokes: [...this.state.jokes, ...newJokes],  loading: false}, this.setLocalStorage)
    }
    handleClick(){
        this.setState({loading: true}, this.getJokes)
    }
    componentDidMount(){
        this.getJokes()    
    }
    setLocalStorage(){
        window.localStorage.setItem("jokes", JSON.stringify(this.state.jokes))
    }
    handleVote(id, vote){
        this.setState(st => {
            return {jokes: st.jokes.map(joke => joke.id === id ? {...joke, votes: joke.votes + vote} : joke)}
        }, this.setLocalStorage)
    }

    render(){
        if (this.state.loading){
            return (
                <div className = {"DadJokes-loading"}>
                    <i className="fas fa-sync fa-spin"></i>
                    <h1>Loading</h1>
                </div>
            )
        }
        let jokes = this.state.jokes.sort((a, b) => b.votes - a.votes)
        return(
            <div className = {"DadJokes"}>

                <div className={"DadJokes-sidebar"}>
                    <h1 className = {"DadJokes-title"}><span>Dad</span> Jokes</h1>
                    <img src = {"https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Emojione_1F602.svg/1024px-Emojione_1F602.svg.png"} alt="Laughing emoji"/>
                    <button onClick = {this.handleClick} className = {"DadJokes-newJokes"}>Summon more Jokes</button>
                </div>

                <div className="DadJokes-list">
                    {jokes.map((joke) => 
                        <Joke 
                            key={joke.id}
                            id={joke.id}
                            text={joke.text}
                            votes={joke.votes}
                            upvote={joke.upvote}
                            downvote={joke.downvote}
                        />
                    )}
                </div>
            </div>
        )
    }
}

export default DadJokes